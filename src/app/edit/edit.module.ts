import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";

import { EditRoutingModule } from "./edit-routing.module";
import { EditComponent } from "./edit.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        EditRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        EditComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class EditModule { }
