import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as Toast from "nativescript-toast";
import * as dialogs from "@nativescript/core/ui/dialogs";
import { RouterExtensions } from "@nativescript/angular";
let localStorage = require( "nativescript-localstorage" );

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    texto: string;

    constructor(private enrutador: RouterExtensions) {
    }

    doLater(fn){setTimeout(fn, 1000);}

    ngOnInit(): void {
        this.texto = localStorage.getItem('nombreUsuario', 'John');
        /* this.doLater(() => {
            dialogs.action("Mensaje", "Cancelar", ["Opcion 1", "Opcion 2"])
                .then((result) => {
                    console.log("resultado " + result);
                    if(result === "Opcion 1"){
                        this.doLater(() => dialogs.alert({
                            title: "Titulo 1",
                            message: "msg 1",
                            okButtonText: "btn 1"
                        }).then(() => console.log("Cerrado 1!")));
                    }else if(result === "Opcion 2"){
                        this.doLater(() => dialogs.alert({
                            title: "Titulo 2",
                            message: "msg 2",
                            okButtonText: "btn 2"
                        }).then(() => console.log("Cerrado 2!")));
                    }
                });
        }); */
        var toast = Toast.makeText("Hola Mundo", "long");
        this.doLater(() => toast.show());
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onclick(e){
        this.enrutador.navigate(['./edit']);
    }
}
