import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, View } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { android, ios } from "@nativescript/core/application";
import { RouterExtensions } from "@nativescript/angular";
import * as Toast from "nativescript-toast";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"/* ,
    providers: [NoticiasService] */
})
export class SearchComponent implements OnInit {
    resultados: Array<string> = [];
    @ViewChild("layout") layout: ElementRef;

    constructor(public noticias: NoticiasService, private enrutador: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        
        /* this.noticias.agregar('Noticia 1');
        this.noticias.agregar('Noticia 2');
        this.noticias.agregar('Noticia 3');
        if(android){
            this.noticias.agregar('Noticia de Android');
        }
        if(ios){
            this.noticias.agregar('Noticia de iOS');
        } */
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x):void{
        this.enrutador.navigate(['./contact']);
    }

    buscarAhora(s: string){
        // this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        this.noticias.buscar(s).then((r: any) => {
            console.log("Resultados (" + s + "): " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("Error: " + e);
            var toast = Toast.makeText("Error en la busqueda", "long");
            toast.show();
        });

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            opacity: 0.5,
            duration: 1000,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("lightseagreen"),
            opacity: 1,
            duration: 3000,
            delay: 150
        }));
    }
}
