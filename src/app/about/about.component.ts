import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import * as dialogs from "@nativescript/core/ui/dialogs";

@Component({
    selector: "About",
    templateUrl: "./about.component.html"
})
export class AboutComponent implements OnInit {
    resultados: Array<string> = [];

    constructor(public noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    doLater(fn){setTimeout(fn, 200);}

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.resultados.push("Item " + this.resultados.length);
            pullRefresh.refreshing = false;
        }, 2000);
    }

    onclick(e, x){
        this.doLater(() => {
            if(e.object.text === "Eliminar"){
                dialogs.confirm('Se va a eliminar el item').then((result) => {
                    if(result){
                        for(var i = 0; i < this.resultados.length; i++){
                            if(this.resultados[i] === x){
                                this.resultados.splice(i, 1);
                            }
                        }
                    }
                });
            }
            else if(e.object.text === "Editar"){
                dialogs.prompt("Escriba el nuevo nombre del item", x).then((r) => {
                    for(var i = 0; i < this.resultados.length; i++){
                        if(this.resultados[i] === x){
                            this.resultados[i] = r.text;
                        }
                    }
                });
            }
        });
    }
       
}
