import { Injectable } from "@angular/core";
import { getJSON, request } from "@nativescript/core/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService{
    api: string = "https://2ffb8adfbb3e.ngrok.io";

    constructor(){
        this.getDB((db) => {
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("Fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("Error on getDB"));
    }

    getDB(fnOk, fnEr){
        return new sqlite("mi_db_logs", (err, db) => {
            if(err){
                console.error("Error al abrir db", err);
            }else{
                console.log("Esta la db abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnEr(error);
                    });
            }
        });
    }

    agregar(s: string){
        return request({
            url: this.api + "/favs",
            method: "POST", headers: {"Content-Type": "application/json"},
            content: JSON.stringify({nuevo: s})
        });
    }

    favs(){
        return getJSON(this.api + "/favs");
    }

    buscar(s: string){
        this.getDB((db) => {
            db.execSQL("INSERT INTO logs (texto) VALUES (?)", [s],
                (err, id) => console.log("Nuevo id: ", id));
        }, () => console.error("Error on getDB: "));
        return getJSON(this.api + "/get?q=" + s);
    }

    /* buscar(s: string){
        console.log("Llamando al API " + s);
        return getJSON(this.api + "/get?q=" + s);
    } */
    /* private noticias: Array<string> = [];

    agregar(s: string){
        this.noticias.push(s);
    }

    buscar(){
        return this.noticias;
    } */
}
