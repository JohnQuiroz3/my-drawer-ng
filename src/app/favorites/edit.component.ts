import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { RouterExtensions } from "@nativescript/angular";
let localStorage = require( "nativescript-localstorage" );

@Component({
    selector: "Edit",
    moduleId: module.id,
    templateUrl: "./edit.component.html"
})
export class EditComponent implements OnInit {
    textFieldValue: string = "";

    constructor(private enrutador: RouterExtensions) {
        this.textFieldValue = localStorage.getItem('nombreUsuario');
    }

    ngOnInit(): void {
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    doLater(fn){setTimeout(fn, 200);}

    onclick(e){
        localStorage.setItem('nombreUsuario', this.textFieldValue);
        this.enrutador.navigate(['./settings']);
    }
}
